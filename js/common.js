$(document).ready(function () {

  //Каруселька
  //Документация: http://kenwheeler.github.io/slick/
  $('.slick-carousel').slick({
   // autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
    dots: true,
    dotsClass: 'indicators'
  });
  

  // Табы
  var tabs = $('#tabs');
  $('.tabs-content > div', tabs).each(function (i) {
    if (i != 0) $(this).hide(0);
  });
  tabs.on('click', '.tabs a', function (e) {
    e.preventDefault();
    var tabId = $(this).attr('href');
    $('.tabs .tab').removeClass('active');
    $(this).closest('.tab').addClass('active');
    $('.tabs-content > div', tabs).hide(0);
    $(tabId).show();
  });

  // Селекты
  $(function () {
    $('#category').chained('#region');
    $('#date').chained('#category');
    $('#price').chained('#date');
  })

  // Menu

  var touch = $('#touch-menu');
  var menu = $('nav');

  $(touch).on('click', function (e) {
    e.preventDefault();
    menu.slideToggle();
  });

});